(require '[hiccup2.core :as h]
         '[clojure.string :as strings]
         '[babashka.cli :as cli])


(defn get-environ [name]
  (. System getenv name))

(def git-hash (get-environ "GIT_HASH"))
(def last-updated (Long/parseLong (or (get-environ "LAST_UPDATED") "0")))

(def info {
           :title "Programmation Client II"
           :github "https://github.com/didiercrunch/cours-420-301-ah"})



(def header 
  [:head
    [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
    [:meta {:http-equiv "X-UA-Compatible" :content "ie=edge"}]
    [:meta {:charset "utf-8"}]
    [:link {:rel "stylesheet" :href "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"}]
    [:link {:rel "stylesheet" :href "css/paper.min.css"}]])

(defn extensionless-file-name [name]
  (let [join-with-space #(strings/join "." %)]
    (-> name str (strings/split #"\/") last (strings/split #"\.") butlast join-with-space)))


(defn file-link [file]
  [:a {:href (str file)} (extensionless-file-name file)])

(defn replace-slides-to-html [file]
  (strings/replace file #"slides\.html$" "html"))

(defn execices-for [exercices presentation-file]
  (let [presentation-file (extensionless-file-name presentation-file)
        for-presentation-file? #(strings/starts-with? presentation-file (extensionless-file-name %))]
    (filter for-presentation-file? exercices)))

(defn presentation-link [file exercices]
  [:li {:style {:margin-bottom "0.5em"}}  
    [:span 
      (file-link file) 
      [:a {:href (replace-slides-to-html file) :style {:margin-left "2em" :margin-right "1em"}} [:i.fa-brands.fa-html5]]
      [:a {:href  (str file "?print-pdf")} [:i.fa-solid.fa-print {:style {:margin-left "0.5em"}}]]]
    [:ul.inline 
      (for [ex (execices-for exercices file)]
        [:li [:a {:href (str ex)} "Exercices"]])]])

(def colophon 
  (let [project-url "https://gitlab.com/didier-ahuntsic/cours-420-301-ah"
        style {:style {:font-size "0.75em"}}]
    [:div
      [:span style "Sources: " [:a {:href project-url} project-url]]
      [:br]
      [:span style "Dernière Mise-à-Jour: " (-> last-updated (* 1000) java.util.Date. str)]
      [:br]
      [:span style "Identificant Git: " [:it "#" git-hash]]]))
      

(defn body [{courses :courses exams :exams exercices :exercices}]
  [:div.paper.container {:style {:margin-top "3em"}}
    [:h2 (:title info)]
    [:h3 "Notes De Cours"]
    [:div
      [:h4 "Présentations"]
      [:ul (for [file courses] (presentation-link file exercices))]]
    [:div
      (when (seq exams) 
        [:div
          [:h4 "Examens"]
          [:ul (for [file exams] [:li (file-link file)])]])]
    #_([:div
      [:h4 "Cheat Sheet"]
      [:ul 
       [:li [:a {:href "https://training.github.com/downloads/fr/github-git-cheat-sheet/"} "https://training.github.com/downloads/fr/github-git-cheat-sheet/"]]]])
    [:div
      [:h4 "Repository Git"]
      [:ul 
        [:li [:a {:href (:github info)} (:github info)] " est le projet utilisé durant le cours."]]]  
    [:div {:style {:margin-top "5em"}} 
      [:hr]
      colophon]])

(defn generate-index-html [opts]
  (h/html {:mode :html} 
    [:html {:lang "en"}
        header
        (body opts)]))

(defn prepend-doctype [doc]
  (str "<!DOCTYPE html>" doc))

(def cli-options {:coerce {:courses [] :exams [] :exercices []}})

(->  *command-line-args*  
     (cli/parse-opts cli-options)
     generate-index-html 
     str 
     prepend-doctype 
     println)





