#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.split(regex("\\.")).slice(1).join(".")
}

#set heading(numbering: numbering-function, level: 1)

== Personne 

=== Créer une classe `Personne`
Créer une classe personne qui prend en argument le nom, le prenom ainsi que la date de naissance.

```js 
const person = new Personne("bob", "dylan" new Date(1941, 4, 3));

```

=== Méthode age 

Créer une méthode qui retourne l'age d'une personne.

```js 
const person = new Personne("bob", "dylan" new Date(1941, 4, 3));
console.log(personne.getAge()); // 85
```


== Triangle 

La classe `Point` peut être importé grace à 
```js
import {Point} from "https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@006.3/modules-examples/mod.js";
```

https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@006.3/modules-examples/mod.js



=== Créer une classe `Triangle`
Créer une classe Triangle.  Le constructeur de triangle doit prendre trois `Point` en paramêtres.  Créer une instance devrait se faire ainsi.

```js
let p1 = new Point(0, 0);
let p2 = new Point(0, 1);
let p3 = new Point(1, 0);
let triangle = new Triangle(p1, p2, p3);
```

=== Méthode `toString()`

Créer une méthode `toString()` qui retourne un joly string qui représente le triangle.

```js
let p1 = new Point(0, 0);
let p2 = new Point(0, 1);
let p3 = new Point(1, 0);
let triangle = new Triangle(p1, p2, p3);
console.log(triangle.toString()); // Triangle: (0,0), (0, 1) et (1, 0)
```


=== Translation du cercle

Créer méthode `tanslate(point)` qui translate le triangle de `point.getX()` vers la droite et
`point.getY()` vers le haut.



== Résultat de parties de balle-molle

=== Partie de balle molle

==== Créer une classe `PartieDeBalleMolle`

Créer une classe "partie de balle molle" qui représente le résultat d'une partie.
Le résultat d'une partie de balle molle est constitué du nom de l'équipe à domicile,
du nom de l'équipe visiteuse ainsi que du pointage de chacune des équipes.

```js 
const partie = new PartieDeBalleMolle("bleu", 11, "rouge", 9);
```


=== Créer des "getters"

Pour chaque champ, créer un getter pour permettre d'extraire
l'information d'une instance.

```js 
const partie = new PartieDeBalleMolle("bleu", 11, "rouge", 9);

console.log(partie.getHomeTeam());  // "bleu"
console.log(partie.getAwayTeam());  // "rouge"
console.log(partie.getHomeScore()); // 11
console.log(partie.getAwayScore()); // 9
```

=== Résultats

Il va falloir créer une classe qui enrobe une liste de `PartieDeBalleMolle`.  L'objectif de cette 
classe est d'être capable d'exprimer les statistiques d'une saison de balle molle.

==== Créer classe 
Créer une classe simple `SaisonBalleMolle` qui "enrobe" une liste de `PartieDeBalleMolle`.

```js 
const saison = new SaisonBalleMolle();
```

=== Ajouter des parties 
Créer une méthode qui permet d'ajouter une `PartieDeBalleMolle` à une `SaisonBalleMolle`.
```js 
const saison = new SaisonBalleMolle();
const partie = new PartieDeBalleMolle("bleu", 11, "rouge", 9);
saison.addMatch(partie);
```

=== Nombre totale de parties 
Créer une méthode qui permet de savoir combien de partie au total la `SaisonBalleMolle` contient.

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 9);
const partie2 = new PartieDeBalleMolle("vert", 23, "jaune", 21);

saison.addMatch(partie1);
console.log(saison.totalNumberOfPlayedGames()); // 1
saison.addMatch(partie2);
console.log(saison.totalNumberOfPlayedGames()); // 2
```

=== Équipes qui ont jouées

Créer une méthode qui retourne le nom de toutes les équipes qui ont jouées.

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 9);
const partie2 = new PartieDeBalleMolle("vert", 23, "jaune", 21);

saison.addMatch(partie1);
console.log(saison.listTeams()); // ["bleu", "rouge"]
saison.addMatch(partie2);
console.log(saison.listTeams()); // ["bleu", "rouge", "vert", "jaune"]
```


=== Parties jouées 

Écrire une méthode qui prend en paramêtres le nom d'une équipe et retourne le nombre 
de parties jouées par l'équipe. 

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 9);

console.log(saison.getNumberOfPlayedGames("bleu")); // 0

saison.addMatch(partie1);
console.log(saison.getNumberOfPlayedGames("bleu")); // 1
```


=== Parties gagnées 

Écrire une méthode qui prend en paramêtres le nom d'une équipe et retourne le nombre 
de partie gagnées par l'équipe. 

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 9);

console.log(saison.getNumberOfWonGames("bleu")); // 0

saison.addMatch(partie1);
console.log(saison.getNumberOfWonGames("bleu")); // 1
console.log(saison.getNumberOfWonGames("rouge")); // 0
```



=== Parties perdues 

Écrire une méthode qui prend en paramêtres le nom d'une équipe et retourne le nombre 
de partie perdues par l'équipe. 

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 9);

console.log(saison.getNumberOfLostGames("bleu")); // 0

saison.addMatch(partie1);
console.log(saison.getNumberOfLostGames("bleu")); // 0
console.log(saison.getNumberOfLostGames("rouge")); // 1
```


=== Parties nulles 

Écrire une méthode qui prend en paramêtres le nom d'une équipe et retourne le nombre 
de partie nulles par l'équipe. 

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 11);

console.log(saison.getNumberOfTiedGames("bleu")); // 0

saison.addMatch(partie1);
console.log(saison.getNumberOfTiedGames("bleu")); // 1
console.log(saison.getNumberOfTiedGames("rouge")); // 1
```

=== Points pour

Écrire une méthode qui prend en paramêtres le nom d'une équipe et retourne le nombre total
marqué par l'équipe.

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 11);
const partie2 = new PartieDeBalleMolle("bleu", 5, "vert", 6);

console.log(saison.getNumberPointsFor("bleu")); // 0

saison.addMatch(partie1);
console.log(saison.getNumberPointsFor("bleu")); // 11

saison.addMatch(partie2);
console.log(saison.getNumberPointsFor("bleu")); // 16
```


=== Points contres

Écrire une méthode qui prend en paramêtres le nom d'une équipe et retourne le nombre total
marqué contre l'équipe.

```js 
const saison = new SaisonBalleMolle();
const partie1 = new PartieDeBalleMolle("bleu", 11, "rouge", 11);
const partie2 = new PartieDeBalleMolle("bleu", 5, "vert", 6);

console.log(saison.getNumberPointsFor("bleu")); // 0

saison.addMatch(partie1);
console.log(saison.getNumberPointsFor("bleu")); // 11

saison.addMatch(partie2);
console.log(saison.getNumberPointsFor("bleu")); // 17
```











