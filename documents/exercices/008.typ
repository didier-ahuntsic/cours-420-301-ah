#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.split(regex("\\.")).slice(1).join(".")
}

#set heading(numbering: numbering-function, level: 1)

==  

Toutes les questions ci-bas se basent sur https://bit.ly/3NVZ91Z.  Dans chaque question,
vous devez remplacer les `???` par les valeurs appropriées.



=== 

```js 
const player = new NPC("no name", 100);
player.takeHit(10, 25);
player.takeHit(20, 10);
assertEquals(player.pointOfLife, ???);
```



=== 

```js 
const player = new NPC("no name", 100);
player.takeHit(100, 25);
player.takeHit(20, 10);
assertEquals(player.isDead(), ???);
```

=== 

```js 
const player = new Ninja("no name", 100);
player.takeHit(10, 25);
player.takeHit(15, 25);
player.takeHit(25, 15);
assertEquals(player.pointOfLife, ???);
```



=== 

```js 
const player = new Ninja("no name", 100);
player.takeHit(10, 25);
player.takeHit(15, 25);
player.takeHit(25, 15);
assertEquals(player.pointOfLife, ???);
```

===

```js
const player = new Mage("Hugette La Chouette", 100, 100);
player.takeHit(10, 25);
player.takeHit(15, 25);
player.takeHit(25, 15);
assertEquals(player.pointOfLife, ???);
assertEquals(player.mana, ???);
```

===

```js
const player = new GrandWizard("no name", 100, 100);
player.takeHit(5, 25);
player.takeHit(15, 25);
player.takeHit(25, 15);
assertEquals(player.pointOfLife, ???);
assertEquals(player.mana, ???);
```


===

```js
const player = new Berserker("no name", 100, 100);
player.takeHit(5, 25);
player.takeHit(15, 25);
player.takeHit(25, 15);
assertEquals(player.pointOfLife,  ???);
```

===

```js
const fight = new Fight(
    new Mage("Edourd LeRoux", 100, 100),
    new Mage("Pierre Peterson", 100, 100)
    );
fight.player1Attack();
fight.player2Attack();
assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);
fight.player1Attack();
fight.player2Attack();
assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.player2Attack();
assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.player2Attack();
assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);
```


===

```js
const fight = new Fight(
    new Mage("Edourd LeRoux", 100, 100),
    new Ninja("Robert El Muy Rapido", 100)
    );

assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.movePlayer2(fight.player1Position + 11);

assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.player2Attack();
assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.player2Attack();
assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);
```

===

```js
const fight = new Fight(new Mage("Edourd LeRoux", 100, 100),
        new Berserker("Manon L'Enrager", 100, 100));

assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.player2Attack();

assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.movePlayer2(fight.player1Position + 5);

assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);

fight.player1Attack();
fight.player2Attack();

assertEquals(fight.player1.pointOfLife, ???);
assertEquals(fight.player2.pointOfLife, ???);
```