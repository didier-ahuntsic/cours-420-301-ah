#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


#set heading(numbering: numbering-function, level: 1)


===
Quelle heure est-il localement?

===
Quelle mois sommes-nous présentement? Écrire le mois mot.

===
Quelle année sommes-nous?

===
Quel est l'époch time présentement (en seconde)?

===
Quelles minutes dans l'heure est-il présentement?

===
Dans quel fuseau horaire vous trouvez vous présentement (en heure par rapport à UTC-0)?

===
Quelle est l'epoch time dans exactement 1 an?

===
Quelle était l'époch time à ce moment exact le mois passé?
