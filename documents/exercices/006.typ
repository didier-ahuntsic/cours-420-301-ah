#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


La classe `HockeyPlayerStatistics` peut être importé grace à 
```js
import {HockeyPlayerStatistics} from "https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@006.3/modules-examples/mod.js";
```

https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@006.3/modules-examples/mod.js

#set heading(numbering: numbering-function, level: 1)


===
Créer une instance de `HockeyPlayerStatistics` avec le joueur 
"Wayne Gretzky" (née le  26 janvier de l'an de grâce 1961 à Brantford).

astuce:
Regarder les différent constructeurs offert par la classe "Date"
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/Date

===
Quel age à Wayne Gretzky aujourd'hui?

===
Wayne Gretzky a jouer 1 487	parties et compté 2 857	points.  Quelle est
ça statistique de nombre de point par match?

===
Wayne Gretzky a joué 208 parties en éliminatoire et compté 382 points lors de celles-ci.  Quelle est ça statistique de nombre de point par match éliminatoire?


===
Wayne Gretzky a jouer 1 487	parties et compté 894 buts.  Quelle est
ça statistique de nombre de but par match?


=== 
Refaire les exercices du haut avec les "Mario Lemieux", "Marcel Dionne" et 
"Sidney Crosby".