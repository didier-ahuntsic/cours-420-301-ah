#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


#set heading(numbering: numbering-function, level: 1)


=== Créer une page html

Créer une page web "vide" avec le "body" contenant le code suivant.

```html
<div>
    <input type="text" id="input-1" />
    <div>
        <button id="button-1">button-1</button>
    </div>
    <div>
        <button id="button-2">button-2</button>
    </div>
    <div>
        <button id="button-3">button-3</button>
    </div>
    <div>
        <button id="button-4">button-4</button>
    </div>

    <pre id="output-1"></pre>
</div>
```


https://gist.github.com/didiercrunch/2d385dbdaec536fe211e1d2c0e3d4eb0



=== Créer un module javascript

Créer un fichier script javascript contenant le hello world (`alert("hello world");`)
et utiliser le script dans le fichier html en tant que module.


=== Copier input dans output 

Ajouter un événement "click" sur le boutton `#button-1` qui fait en sorte que le contenue de `#input-1`
soit copier dans `#output-1`.

=== Vérification d'un isbn

Utiliser la fonction `isValidIsbn` du module `https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@latest/modules-examples/books.js` pour que, lorsque l'utilisateur clique
sur le boutton `button-2`, l'output `output-1` afficher "ISBN valide" si l'input est 
un ISBN valide et "ISBN non valide" dans le cas contraire.

*Liste d'ISBN *

- 0-7754-2153-7
- 0-5220-5123-5
- 978-5-0583-5600-2
- 978-8-9094-3382-2

https://gist.github.com/didiercrunch/3f8cf4d6daf09b59ea8d04bb0e3eae29


=== Vérification d'un isbn

Utiliser les fonction `isValidIsbn10` et `isValidIsbn13` du module `https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@latest/modules-examples/books.js` pour que, lorsque l'utilisateur clique
sur le boutton `button-2`, l'output `output-1` afficher "ISBN 13 valide" si l'input est un ISBN 13 valide, "ISBN 10 valide" si l'input est un ISBN 10 valide et "ISBN non valide" dans le cas contraire.

=== Créer un module util
Créer un module javascript nommé `util.js` à côté de votre module d'entré.


=== Ajouter une fonction publique

Ajouter une fonction publique suivante dans votre module `util.js`.


```js 
function isBalanced(str){
  let counter = 0;
  for(const letter of str){
    if(letter === "("){
      counter += 1;
    }
    if(letter === ")"){
      counter -= 1;
    }
    if(counter < 0){
      return false;
    }
  }
  return counter === 0;
}
```

https://gist.github.com/didiercrunch/89521b3a7360cc17f6266b1e038ff4c7

=== Utiliser votre fonction publique 

Utiliser votre fonction publique pour que lorsque l'utilisateur clique sur le boutton `#button-4`,
l'output `#output-1` affiche "Les parenthèses sont balancées" si la valeur de l'input `#input-1` 
est un text où les parenthèses sont balancées et "Les parenthèses ne sont pas balancées" dans le
cas contraire.



