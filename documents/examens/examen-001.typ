#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "Examen I | J'aime le JavaScript", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.A.i", ..nums);
    full.split(regex("\\.")).slice(1).join(".")
}

= Instruction

- Vous n'avez le droit à aucune aide pour faire ce quizz.
- Bonne chance


#set heading(numbering: numbering-function, level: 1)

== Petites Questions 

=== #points(none, 2.5, points-state)

Que représente, approximativement, un Unix timestamp?

=== #points(none, 2.5, points-state)

Quelle méthode est appelée lorsque nous faisons `new Foo()`?

=== #points(none, 2.5, points-state)

Comment faire pour importer la fonction `average` de `https://example.com/stats.js`?

=== #points(none, 2.5, points-state)

Comment nomme-t-on les "fonctions" contenues dans les classes?

== Question de classes

```js 
class Livre{

  constructor(titre, auteur, isbn){
    this.titre = titre;
    this.auteur = auteur;
    this.isbn = isbn;
  }

  getIsbn(){
    return this.isbn;
  }

  getAuteur(){
    return this.auteur;
  }
}

class Bibliotheque{

  constructor(){
    this.livres = [];
  }


  ajouterNouveauLivre(livre){
    this.livres.push(livre);
  }

  getLivres(){
    return this.livres;
  }
}

```



=== #points(none, 10, points-state)

En utilisant les classes ci-haut, créer une `Bibliotheque` contenant les 3 livres suivants:

- J'aime JavaScript, Paul Le Geek, 111-555-999
- J'adoreJs, Manon Grosse-Lunette, 666-555-444
- De rien à JS, Anastasia Moustache, 777-666-555


=== #points(none, 10, points-state)
Écrire une fonction qui prend en paramètre une `Bibliotheque` et un isbn et
qui retourne le livre de la `Bibliotheque` associé à cet isbn.


=== #points(none, 10, points-state)

Écrire une fonction qui prend en paramètre une `Bibliotheque` et un nom d'auteur et
qui retourne les livres de la `Bibliotheque` associés à l'auteur.


=== #points(none, 10, points-state)

Écrire une méthode `toString()` dans `Livre` et qui retourne un beau string
représentant le livre (le string doit include le titre et l'auteur du livre).


<end>