// tp-3

#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#let github-link = link("https://github.com/didiercrunch/dessin")
#let inkscape = link("https://inkscape.org/")[inkscape]

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique III | Dessins d'adultes", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}

= Instruction

- Ce deuxième devoir doit être remis pour le 2 février 2024 à 18h00.
- Le travail doit être fait en équipe de 1 ou 2.
- Le travail doit être remis sous la forme zip par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


=== Bonne chance
Si vous avez des question, n'hésite pas à contacter votre enseignant au
didier.amyot\@collegeahuntsic.qc.ca ou par mio.

== Quoi remettre

Le dossier compréssé à remettre doit comprendre un README où vous écriverez
vos noms et numéros d'étudiants.  Tout le code doit se trouver dans un sous
dossier `src`.  La page web principale doit se nommer `index.html`.  Ici 
est un exemple du résultat.

```
README.md
src/
├─ index.html
├─ main.js

```

= Énoncé

Ce travail consiste à ajouter des fonctionnalités au projet #github-link.  Les 
fonctionnalités sont décrite en language courant.  Vous êtes responsable de
traduire les fonctionnalités demandés en requis techniques et d'implémenter 
vous solutions.


== Projet Générale

Le projet #github-link est un logiciel de dessin un peu comme #inkscape, au tout début
de son implémentation.  L'objectif de se travail est j'ajouter des fonctionnalités sans 
enlever les fonctionnalités déjà implémentées.


== Grille de correction

1. implémentation correcte de la fonctionnalités

2. Élégence du code


== Fonctionnalités

#set heading(numbering: numbering-function, level: 1)

=== #points(none, 30, points-state) Persistance inter-rafraichessement

En tant qu'utilisateur, je veux être capable rafraichir mon navigateur
sans perdre l'état de mon dessin.

*Astuce*

Ajouter des méthodes `fromJson(json)` et `toJson()` à vos formes.



=== #points(none, 15, points-state) Filtrer les formes

En tant qu'utilisateur, je veux être capable de filtrer le tableau 
des formes par type de formes.  Par exemple, je veux être capable 
de juste avoir les rectangles dans le tableau du bas.


=== #points(none, 15, points-state) Transparence des formes

En tant qu'utilisateur, je veux être capable d'ajuster la transparence
de mes formes de la même manière que je peux ajuster la couleur.



=== #points(none, 10, points-state) Beaux identifiants

En tant qu'utilisateur, je veux voir de beaux identifiants (1, 2, ...)
et non des unix tempstemps.




<end>