#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique I | Dessins d'Enfants", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}

= Instruction

- Ce deuxième devoir doit être remis pour le 24 decembre 2023 à 13h00.
- Le  travail doit être fait en équipe de 1 ou 2.
- Le travail doit être remis sous la forme zip par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


=== Bonne chance
Si vous avez des question, n’hésite pas à contacter votre enseignant au
didier.amyot\@collegeahuntsic.qc.ca ou par mio.

== Quoi remettre

Le dossier compréssé à remettre doit comprendre un README où vous écriverez
vos noms et numéro d'étudiants.  Tout le code doit se trouver dans un sous
dossier `src`.  La page web principale doit se nommer `index.html`.  Ici 
est un exemple du résultat.

```
README.md
src/
├─ index.html
├─ main.js

```

= Énoncé

=== Objectif
L'objectif de ce travail pratique est de créer un logiciel de dessin. Pour l'instant,
nous allons seulement supporter la création de cercles.  L'utilisateur devra entrer le point 
central du cercle ainsi que le rayon du cercle.  Le logiciel montrera le dessin dans 
un format SVG (https://en.wikipedia.org/wiki/SVG).

=== Documentation 
La documentation de toutes les classes et fonctions que vous devez utiliser pour ce travail 
est accessible sur https://didiercrunch.github.io/cours-420-301-ah/.

Les classes et fonctions que vous allez avoir besoin pour accomplir votre tâche sont les suivantes:

- #link("https://didiercrunch.github.io/cours-420-301-ah/#drawcircles")[drawcircles]
- #link("https://didiercrunch.github.io/cours-420-301-ah/#populateTable")[populateTable]
- #link("https://didiercrunch.github.io/cours-420-301-ah/#randomColor")[randomColor]
- #link("https://didiercrunch.github.io/cours-420-301-ah/#Circle")[Circle]
- #link("https://didiercrunch.github.io/cours-420-301-ah/#Point")[Point]

=== Exemple 
Un exemple du travail final est accessible sur 
https://didier-ahuntsic.gitlab.io/cours-420-301-ah/examples/tp-001/index.html


#set heading(numbering: numbering-function, level: 1)

=== #points(none, 5, points-state) Création du dossier de base

Créer un dossier avec les fichiers suivant:

- Le dossier doit contenir un `README.md` contenant vos noms et numéro d'étudiants.  

- Vous devez copier le contenue de https://gitlab.com/didier-ahuntsic/cours-420-301-ah/-/blob/main/cours/examples/tp-001/index.html pour créer le fichier `src/index.html`.

- Vous devez créer un fichier `src/main.js` qui soit utilisé comme un 
  js module par `index.html`. Le fichier `main.js` sera le fichier où le
  reste de votre travail sera effectué.

Le résultat doit ressembler à quelque chose comme:

```
README.md
src/
├─ index.html
├─ main.js
```

=== #points(none, 10, points-state) Dessiner un cercle simple
Lorsque l'utilisateur clique sur le boutton "Cercle Simple", 
un cercle centré en $(250, 250)$ et de rayon $50$ doit être 
dessiné sur le canevas.

Pour faire cette question, vous allez avoir besoin des classes 
`Point` et `Circle` ainsi que de la fonction `drawCircles` du module https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@tp1.1/modules-examples/mod.js .

*truc*


Il sera important pour le reste du travail de 
choisir judicieusement les valeurs des ids de cercles.  Il faut 
impérativement que chaque cercle aille un id unique.


=== #points(none, 20, points-state) Ajouter un cercle
Lorsque l'utilisateur clique sur "Ajouter Cercle", 
il faut qu'un nouveau cercle soit dessiné sur le canevas.  On peut présumer
que les valeurs entrées par l'utilisateur sont des nombres valides.

Pour faire cette question, vous allez avoir besoin des classes 
`Point` et `Circle` ainsi que de la fonction `drawCircles` du module https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@tp1.1/modules-examples/mod.js .


*truc* 

Vous allez devoir créer une liste globale contenant tous les cercles .  
Lorsque l'utilisateur clique sur le bouton "Ajouter
Cercle" il devrait se passer deux choses:

1. un nouveau cercle devrait être ajouté à la liste globale.
2. tous les cercles de la liste globale devront être dessinés.


=== #points(none, 10, points-state) Ajouter le tableau du bas

Lorsque l'utilisateur clique sur "Ajouter Cercle" (et en même temps 
que le rendu du dessin), il faut que le contenu du tableau représente
l'état du dessin.

Pour faire cette question, vous allez avoir besoin des classes 
`Point` et `Circle` ainsi que de la fonction `populateTable` du module
 https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@tp1.1/modules-examples/mod.js .

Cette question ne requiert de passer aucun argument optionnel à la fonction `populateTable`.

*truc* 

Écrivez une fonction qui dessine le dessin et le tableau en même temps.

=== #points(none, 5, points-state) Colonne "centre" du tableau du bas

L'objectif de cette question est de remplir la colonne "Centre" du 
tableau du bas. Pour ce faire, utiliser l'argument optionnel `pointToString` de
`populateTable` pour représenter le centre du cercle sous forme 
d'un couple $(x, y)$.

*truc* 
Vous allez devoir passer une fonction en argument à `populateTable`.

=== #points(none, 10, points-state) Effacer tous les cercles

Lorsque l'utilisateur clique sur "Tout Effacer", il faut que le canevas 
et le tableau deviennent vides.

*truc*

Vous allez devoir vider votre liste de cercles globale.


=== #points(none, 20, points-state) Bouger les cercles

Utiliser les arguments optionnels `onClickUp`, `onClickDown`, `onClickLeft`, 
`onClickRight` de `populateTable` pour bouger les cercles de `10px` dans 
chaque direction.

*truc*

- Vous allez devoir créer une fonction `getCercleById` qui prend un 
  `id` et qui retourne le cercle avec cet `id`.

- Vous allez devoir modifier des `Circle` dans la liste globale sans en créer de nouveaux.

- Vous allez devoir appeler votre fonction qui dessine le dessin et 
  le tableau après avoir modifié les cercles.

=== #points(none, 10, points-state) Changer la couleur des cercles

Utiliser l'argument optionnel `onClickPaint` de `populateTable` 
pour changer la couleur des cercles.  

Pour faire cette question, vous allez avoir besoin de la fonction `randomColor` du
module https://cdn.jsdelivr.net/gh/didiercrunch/cours-420-301-ah@tp1.1/modules-examples/mod.js .


=== #points(none, 10, points-state) Inverser les cercles

Lorsque l'utilisateur clique sur "Inverser l'Ordre", il faut que l'ordre
de tous les cercles dans le tableau et le dessin soient inversées.


<end>