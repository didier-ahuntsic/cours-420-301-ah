#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "Examen III | La hiérarchie de classe, c'est cool.", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.A.i", ..nums);
    full.split(regex("\\.")).slice(1).join(".")
}

#let answer-line-2 = table(
  columns: (auto, 1fr),
  inset: 5pt,
  stroke: none,
  align: horizon,
  [Réponse: ], [#answer-line(paint: black)]
)

= Instruction

- Vous n'avez le droit à aucune aide pour faire ce quizz.
- Bonne chance


#set heading(numbering: numbering-function, level: 1)

= Questions 

Considérer le code suivant:

```js 
class Accio{
   constructor(x, y){
       this.x = x;
       this.y = y;
   }
   
   flambios(){
       return this.x + this.y;
   }
   
   impero(){
       return this.x * this.y
   }
}


class Destructum extends Accio {
    constructor(x, y){
        super(x + 10, y - 5)
    }
}


class Mutismus extends Destructum {

    flambios(){
        return super.flambios() + 100
    }
    
    reducto(){
        return super.flambios()
    }

}


class Portus extends Mutismus {

    surdinam(x){
        return this.reducto() + x;
    }

}

class Sonorus extends Portus{

}

class Lumos extends Destructum {
    impero(){
        return super.impero() * 2;
    }
    
    flambios(){
        if(this.impero() < 50){
            return 35;
        }
        return 68;
    }
}

class Failamalle extends Lumos {

    constructor(x, y){
        super(0, 0);
        this.x = 54;
        this.y = 100;
    }
}
```

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Accio(10, 10);
console.log(sort.flambios());
```

#answer-line-2
// 20


== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Destructum(10, 10);
console.log(sort.flambios());
```
// 25

#answer-line-2

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Mutismus(25, 10);
console.log(sort.flambios());
```
// 140
#answer-line-2

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Mutismus(25, 10);
console.log(sort.reducto());
```
#answer-line-2

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Portus(25, 10);
console.log(sort.surdinam(75));
```
#answer-line-2

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Sonorus(10, 25);
console.log(sort.flambios());
```
#answer-line-2

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Lumos(10, 6);
console.log(sort.flambios());
```

#answer-line-2

== #points(none, 10, points-state)

Qu'est-ce qui est imprimé par le code ci-bas:

```js
const sort = new Failamalle(10, 6);
console.log(sort.flambios());
```

#answer-line-2

<end>